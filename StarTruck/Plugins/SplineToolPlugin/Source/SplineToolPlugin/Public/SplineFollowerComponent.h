// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SplinePathComponent.h"
#include "Components/TimelineComponent.h"
#include "SplineFollowerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), BlueprintType)
class SPLINETOOLPLUGIN_API USplineFollowerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USplineFollowerComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineFollower")
	AActor* PathOwner;

	UTimelineComponent* MovementTimeline;
	UCurveFloat *movementCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineFollower")
	float Direction = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineFollower")
	float Velocity = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineFollower")
	float Distance = 1.0f;

	UFUNCTION(BlueprintImplementableEvent)
	void Rotate();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void MovementTick(float);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

};

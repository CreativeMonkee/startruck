// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SplineComponent.h"
#include "EventPoint.h"
#include "SplinePathComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), BlueprintType)
class SPLINETOOLPLUGIN_API USplinePathComponent : public USplineComponent
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineFollower")
	TArray<AEventPoint*> eventPoints;

private:

protected:
};

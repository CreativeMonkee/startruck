// Fill out your copyright notice in the Description page of Project Settings.


#include "EventPoint.h"
#include "SplineFollowerComponent.h"
#include "Engine/Engine.h"

// Sets default values
AEventPoint::AEventPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    collider = CreateDefaultSubobject<USphereComponent>("Collider");
	collider->InitSphereRadius(2.0f);
	collider->SetupAttachment(RootComponent);

	collider->OnComponentBeginOverlap.AddDynamic(this, &AEventPoint::OnOverlapBegin);

}

// Called when the game starts or when spawned
void AEventPoint::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEventPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEventPoint::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
    AActor* OtherActor,
    UPrimitiveComponent* OtherComp,
    int32 OtherBodyIndex,
    bool bFromSweep,
    const FHitResult& SweepResult){

	USplineFollowerComponent* follower = Cast<USplineFollowerComponent>(OtherActor->GetComponentByClass(USplineFollowerComponent::StaticClass()));
	if (follower != nullptr) {
		// Speed Change
		if (Speed != -1.0f) {
			follower->Velocity = Speed;
		}

		// Rotation Change
		if (Rotation != FRotator(-1, -1, -1)) {
			OtherActor->SetActorRotation(Rotation);
		}

		//Event Trigger
		if (EventName != "Event") {
			GEngine->AddOnScreenDebugMessage(-1, 50.f, FColor::Red, TEXT("Event Trigger!"));
		}
	}
}
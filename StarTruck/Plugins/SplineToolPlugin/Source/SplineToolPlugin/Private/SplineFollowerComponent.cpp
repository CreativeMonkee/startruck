// Fill out your copyright notice in the Description page of Project Settings.


#include "SplineFollowerComponent.h"
#include "Engine/Engine.h"

// Sets default values for this component's properties
USplineFollowerComponent::USplineFollowerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;



	// ...
}


// Called when the game starts
void USplineFollowerComponent::BeginPlay()
{
	Super::BeginPlay();

	USplineComponent* Path = Cast<USplineComponent>(PathOwner->GetComponentByClass(USplineComponent::StaticClass()));
	AActor* temp = Cast<AActor>(GetOwner());

	if (Path != nullptr && temp != nullptr) {


		float i = Path->GetNumberOfSplinePoints();
		float leastdistance = 100000000000.0f;
		float key = 0;

		for (float f = 0; f <= i - 1; f++) {
			float dis = FVector::Dist(Path->GetWorldLocationAtSplinePoint(f), temp->GetActorLocation());
			if (dis <= leastdistance) {
				leastdistance = dis;
				key = f;
			}
		}
		FTransform location = Path->GetTransformAtSplineInputKey(key, ESplineCoordinateSpace::World);
		temp->SetActorTransform(location);

	}
}

void USplineFollowerComponent::MovementTick(float deltaTime)
{
	float newVelocity = Velocity * deltaTime;
	Distance += newVelocity;

	USplineComponent* Path = Cast<USplineComponent>(PathOwner->GetComponentByClass(USplineComponent::StaticClass()));
	float TotalDistance = Path->GetDistanceAlongSplineAtSplinePoint(Path->GetNumberOfSplinePoints() - 1);

	AActor* temp = Cast<AActor>(GetOwner());

	if (Direction < 0.0f) {
		temp->SetActorLocation((Path->GetLocationAtDistanceAlongSpline(TotalDistance - Distance, ESplineCoordinateSpace::World)));
	}
	else {
		temp->SetActorLocation(Path->GetLocationAtDistanceAlongSpline(Distance, ESplineCoordinateSpace::World));
	}

	if (Distance >= TotalDistance || Distance <= Path->GetDistanceAlongSplineAtSplinePoint(0)) {
		Direction = Direction * -1.0f;
		Distance = 0.0f;
	}

}

// Called every frame
void USplineFollowerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	MovementTick(DeltaTime);
	// ...
}
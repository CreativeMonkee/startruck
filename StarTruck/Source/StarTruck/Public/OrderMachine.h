// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DrinksEnum.h"
#include "Tray.h"
#include "TrayCompartment.h"
#include "CCustomer.h"
#include "Hologram.h"
#include "OrderMachine.generated.h"

UCLASS()
class STARTRUCK_API AOrderMachine : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AOrderMachine();

	UFUNCTION(BlueprintCallable)
	void TakeOrder(bool iceCream, bool drink, bool burger, bool hotDog, DrinkType drinktype, int32 SittingPoint, ACCustomer* customer);

	UFUNCTION(BlueprintImplementableEvent)
	void SpawnSlip();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString order;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AActor*> traySpawns;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AActor*> hologramSpawns;


	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

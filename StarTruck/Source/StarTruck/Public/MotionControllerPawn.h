// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "VRController.h"
#include "Math/Color.h"
#include "Camera/CameraComponent.h"
#include "HeadMountedDisplay.h"
#include "MotionControllerPawn.generated.h"

UCLASS()
class STARTRUCK_API AMotionControllerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMotionControllerPawn();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USceneComponent* VROrigin;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = chaperone)
	bool bShowChaperone;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AVRController* LeftController;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AVRController* RightController;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FadeOutDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FadeInDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsTeleporting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor TeleportFadeColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ThumbDeadZone;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool RightStickDown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool LeftStickDown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DefaultPlayerHeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool UseControllerRollToRotate;

	UFUNCTION(BlueprintPure, BlueprintImplementableEvent)
	void GetRotationInput(float UpInput, float RightInput, AVRController* AController, FRotator& rotator) const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};

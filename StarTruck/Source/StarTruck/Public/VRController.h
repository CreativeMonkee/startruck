// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SplineMeshComponent.h"
#include "Containers/Array.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"
#include "MotionControllerComponent.h"
#include "PickupInterface.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "XRMotionControllerBase.h"
#include "VRController.generated.h"

// Hand state enum for use with animations
UENUM(BlueprintType)
enum class EHandState : uint8 {
	OPEN = 0 UMETA(DisplayName = "Open"),
	CANGRAB UMETA(DisplayName = "CanGrab"),
	GRAB UMETA(DisplayName = "Grab"),
};

UCLASS()
class STARTRUCK_API AVRController : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AVRController();

	// The root position of the Hands
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USceneComponent* HandRoot;

	// The mesh for the hands
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Mesh)
	USkeletalMeshComponent* HandMesh;

	// The Unreal motion controller component which allows the position of controllers to be applied to objects in games
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* MotionController;

	// Enum for which hand this is "Left or right"
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true)) 
	EControllerHand Hand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPhysicsHandleComponent* PhysicsHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPrimitiveComponent* GrabbedObject;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float OtherItemLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector HandleLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator OtherItemRotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool HasGrabbed = false;

	// Actor that is "Held" by the hand
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* AttachedActor;

	// Sphere collider for grabbing objects
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent* GrabSphere;

	// boolean for use in the teleport code
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isTeleporterActive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<USplineMeshComponent*> SplineMeshes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bLastFrameValidDestination;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsRoomScale;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector TeleportDestination;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsValidTeleportDestination;

	// State of hands grip animation
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EHandState GripState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bWantsToGrip;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator TeleportRotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TeleportLaunchVelocity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator InitialControllerRotation;

	UFUNCTION(BlueprintCallable, Category = "Grabbing")
	void GetActorNearHand(AActor* &NearestActor);

	UFUNCTION(BlueprintCallable, Category = "Grabbing")
	void ReleaseActor();

	UFUNCTION(BlueprintCallable, Category = "Grabbing")
	void GrabActor();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Teleportation")
	void ActivateTeleporter();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Teleportation")
	void DisableTeleporter();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Teleportation")
	void TraceTeleportDestination(bool& success, TArray<FVector>& TracePoints, FVector& NavMeshLocation, FVector& TraceLocation);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Teleportation")
	void ClearArc();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Teleportation")
	void UpdateArcSpline(bool FoundValidLocation, UPARAM(ref) TArray<FVector>&  SplinePoints);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Teleportation")
	void UpdateArcEndpoint(FVector NewLocation, bool ValidLocationFound);

	UFUNCTION(BlueprintPure, BlueprintImplementableEvent, Category = "Teleportation")
	void GetTeleportDestination(FVector& Location, FRotator& Rotation) const;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Room-Scale")
	bool SetupRoomScaleOutline();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Room-Scale")
	void UpdateRoomScaleOutline();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

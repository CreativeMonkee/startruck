// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Engine/Engine.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Math/UnrealMathUtility.h"
#include "TimerManager.h"
#include "OrderMachine.h"
#include "DrinksEnum.h"
#include "Tray.h"
#include "CCustomer.generated.h"

UCLASS()
class STARTRUCK_API ACCustomer : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACCustomer();

	UFUNCTION(BlueprintCallable)
		void RandomiseOrder();

	UFUNCTION(BlueprintCallable)
		float FinishOrder(FOrderComponents orderComp);

	UFUNCTION(BlueprintCallable)
		void GiveOrder();

	UFUNCTION(BlueprintCallable)
		void Leave();

	UFUNCTION(BlueprintCallable)
		void tickDown();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* orderMachineref;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ATray* tray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool drink;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool burger;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool iceCream;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool hotDog;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool drinkCompleted;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 pay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool burgerCompleted;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool hotDogCompleted;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool iceCreamCompleted;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float cost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float impatience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 SittingNumber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		DrinkType drinkType;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FTimerHandle orderTimer;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};

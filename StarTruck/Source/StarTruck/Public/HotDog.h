// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Materials/MaterialInterface.h"
#include "Engine/Engine.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Grill.h"
#include "TimerManager.h"
#include "HotDog.generated.h"

UCLASS()
class STARTRUCK_API AHotDog : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHotDog();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool cooking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Burnt;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool BunColliderUsed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Temp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float CookedLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Cost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float TimeStepCooking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float ColorLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInterface* Material;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInstanceDynamic* MaterialColor;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCapsuleComponent* Collider;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UParticleSystemComponent* SplaterParticles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UParticleSystemComponent* SmokeParticles;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FTimerHandle Timer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AGrill* Grill;

	UFUNCTION(BlueprintCallable)
		void Cook();

	UFUNCTION(BlueprintCallable)
		float CalculateCost(bool burger);

	UFUNCTION(BlueprintCallable)
		void ChangeColor();

	UFUNCTION(BlueprintCallable)
		void OnOverlapEndTop(AActor* OverlappedActor, AActor* OtherActor);
	//void OnOverlapEndTop(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
		void OnOverlapEndBottom(AActor* OverlappedActor, AActor* OtherActor);
	//void OnOverlapEndBottom(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
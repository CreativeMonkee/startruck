// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/Engine.h"
#include "Hologram.generated.h"

UCLASS()
class STARTRUCK_API AHologram : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHologram();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* IceCreamMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* HologramBeamMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* HologramBaseMesh;

	UFUNCTION()
	void SpawnHologram();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Materials/MaterialInterface.h"
#include "Engine/Engine.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Grill.h"
#include "TimerManager.h"
#include "Burger.generated.h"

UCLASS()
class STARTRUCK_API ABurger : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABurger();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool cooking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool BottomBurnt;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool TopBurnt;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool BunColliderUsed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool BunCollider2Used;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Temp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TopCookedLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BottomCookedLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Cost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BottomColorLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeStepCooking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TopColorLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInterface* TopMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInterface* BottomMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInstanceDynamic* BottomMaterialColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInstanceDynamic* TopMaterialColor;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBoxComponent* TopCollider;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBoxComponent* BottomCollider;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FTimerHandle TopTimer;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FTimerHandle BottomTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AGrill* Grill;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystemComponent* SplaterParticles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystemComponent* SmokeParticles;

	UFUNCTION(BlueprintCallable)
	void TopCook();

	UFUNCTION(BlueprintCallable)
	void BottomCook();

	UFUNCTION(BlueprintCallable)
	float CalculateCost(bool burger);

	UFUNCTION(BlueprintCallable)
	void ChangeColor();

	UFUNCTION(BlueprintCallable)
	void OnOverlapEndTop(AActor* OverlappedActor, AActor* OtherActor);

	UFUNCTION(BlueprintCallable)
	void OnOverlapEndBottom(AActor* OverlappedActor, AActor* OtherActor);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
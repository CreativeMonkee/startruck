// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Components/CapsuleComponent.h"
#include "Materializer_Handle.generated.h"

UCLASS()
class STARTRUCK_API AMaterializer_Handle : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMaterializer_Handle();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* MainMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USceneComponent* Origin;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UPhysicsConstraintComponent* HandleConstraint;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UCapsuleComponent* HandleOverlap;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

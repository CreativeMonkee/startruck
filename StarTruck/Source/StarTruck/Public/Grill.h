// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/BoxComponent.h"
//#include "Burger.h"
#include "Engine/Engine.h"
#include "Grill.generated.h"

UCLASS()
class STARTRUCK_API AGrill : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AGrill();

	// Current temperature of the grill
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Temperature;

	// The current gravity scale of the grill
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float GravityValue;

	// The surface collider to get the items that overlap for cooking
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBoxComponent* SurfaceCollider;
	
	// List of pattys that are having their temperature updated
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<AActor*> Pattys;

	// List of HotDogs that are having their temperature updated
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<AActor*> HotDogs;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

};

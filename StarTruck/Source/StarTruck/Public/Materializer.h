// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/ChildActorComponent.h"
#include "VRController.h"
#include "Materials/Material.h"
#include "Burger.h"
#include "HotDog.h"
#include "Cup.h"
#include "Materializer.generated.h"

UCLASS()
class STARTRUCK_API AMaterializer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMaterializer();

	
	// Main mesh of the Materializer
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* MainMachine;

	// Hologram cylinder Mesh
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* HologramCylinder;

	// Front panel hologram mesh
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* Beam;
	
	// Handle mesh may be replaced with a physics enabled child actor at a later date
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* Handle;

	// Left button mesh and collision
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* Button_L;

	// Right button mesh and collision
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* Button_R;

	// point for the objects to spawn at
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USphereComponent* Spawn;

	// capsule component for overlap with the handle will be removed once the physics enabled handle is in
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UCapsuleComponent* HandleOverlap;

	// List of items to be populated in the blueprint
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<TSubclassOf<AActor>> Selections;

	// list of meshes so that i don't have to reload all of them (needs to be redone)
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		TArray<UStaticMeshComponent*> Meshes;

	// the currently selected item to be displayed or spawned
	UPROPERTY(VisibleAnywhere)
		TSubclassOf<AActor> SelectedItem;

	//tracks the position of the selected item within the selections array
	UPROPERTY(VisibleAnywhere)
		int SelectionValue;

	// The mesh of the hologram of the currently selected item
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* HoloMesh;

	// the material to be applied to the holomesh is selected in the blueprint
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInterface* HoloMaterial;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	// overlap functions for interactions
	UFUNCTION()
		virtual void OnBeginOverlapHandle(UPrimitiveComponent* OverlappedComp,  class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
		virtual void OnBeginOverlapRButton(UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
		virtual void OnBeginOverlapLButton(UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
		
};

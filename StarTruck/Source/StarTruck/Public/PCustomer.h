// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Engine/Engine.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Math/UnrealMathUtility.h"
#include "OrderMachine.h"
#include "DrinksEnum.h"
#include "Tray.h"
#include "PCustomer.generated.h"

UCLASS()
class STARTRUCK_API APCustomer : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APCustomer();

	UFUNCTION(BlueprintCallable)
		void RandomiseOrder();

	UFUNCTION(BlueprintCallable)
		void FinishOrder();

	UFUNCTION(BlueprintCallable)
		void GiveOrder();

	UFUNCTION(BlueprintCallable)
		void Leave();

	UFUNCTION()
		void OnOverlap(AActor* OverlappedActor, AActor* OtherActor);


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* orderMachineref;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATray* tray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool drink;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool burger;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool drinkCompleted;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool pay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool burgerCompleted;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float cost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	DrinkType drinkType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};

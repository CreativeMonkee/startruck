// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "DrinksEnum.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UDrinksEnum : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class STARTRUCK_API IDrinksEnum
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
};

UENUM(BlueprintType)
enum class DrinkType : uint8 {
	Drink_None,
	Drink_Red,
	Drink_Green,
	Drink_Blue
};

USTRUCT(BlueprintType)
struct FOrderComponents {

	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	bool burger = false;

	UPROPERTY(BlueprintReadWrite)
	bool drink = false;

	UPROPERTY(BlueprintReadWrite)
	bool hotDog = false;

	UPROPERTY(BlueprintReadWrite)
	bool burgerTopBun = false;

	UPROPERTY(BlueprintReadWrite)
	bool burgerBottomBun = false;

	UPROPERTY(BlueprintReadWrite)
	bool hotDogBun = false;

	UPROPERTY(BlueprintReadWrite)
	float burgerCookedLevel = 0.0f;

	UPROPERTY(BlueprintReadWrite)
	float hotDogCookedLevel = 0.0f;

	UPROPERTY(BlueprintReadWrite)
	float drinkFillLevel = 0.0f;

	UPROPERTY(BlueprintReadWrite)
	DrinkType drinkType;
};



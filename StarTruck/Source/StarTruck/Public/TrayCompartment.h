// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Tray.h"
#include "DrinksEnum.h"
#include "TrayCompartment.generated.h"

UCLASS()
class STARTRUCK_API ATrayCompartment : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATrayCompartment();

	UFUNCTION(BlueprintImplementableEvent)
	ATray* SpawnTray(bool drink, bool burger, bool hotDog, DrinkType drinktype);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

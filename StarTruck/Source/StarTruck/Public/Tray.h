// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/TargetPoint.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Engine/EngineTypes.h"
#include "UObject/NameTypes.h"
#include "Math/Vector.h"
#include "Cup.h"
#include "HotDog.h"
#include "Burger.h"
#include "DrinksEnum.h"
#include "Tray.generated.h"

UCLASS()
class STARTRUCK_API ATray : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATray();

	//UFUNCTION()
	//void OnOverlap(AActor* OverlappedActor, AActor* OtherActor);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector CupPointLocation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector BurgerPointLocation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector FriesPointLocation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* CupMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* BurgerMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* HotDogMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBoxComponent* Collider;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FOrderComponents orderComp;

	UFUNCTION(BlueprintCallable)
	void OnOverlap(AActor* OverlappedActor, AActor* OtherActor);
	//void OnOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
	void TakeOrder(bool drink, bool burger, bool hotDog, DrinkType drinktype);
	
protected: 
	// Called when the game starts or when spawned
	virtual void BeginPlay() override; 

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

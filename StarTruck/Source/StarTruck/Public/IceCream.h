// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "IceCream.generated.h"

UCLASS()
class STARTRUCK_API AIceCream : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIceCream();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* IceCream;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UCapsuleComponent* Collider;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

// Fill out your copyright notice in the Description page of Project Settings.


#include "Burger.h"

// Sets default values
ABurger::ABurger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//bind the overlap events to trigger
	OnActorEndOverlap.AddDynamic(this, &ABurger::OnOverlapEndTop);
	OnActorEndOverlap.AddDynamic(this, &ABurger::OnOverlapEndBottom);

	//create class components and bind to root component
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	TopCollider = CreateDefaultSubobject<UBoxComponent>("TopCollider");
	BottomCollider = CreateDefaultSubobject<UBoxComponent>("BottomCollider");
	SplaterParticles = CreateDefaultSubobject<UParticleSystemComponent>("SplaterParticles");
	SmokeParticles = CreateDefaultSubobject<UParticleSystemComponent>("SmokeParticles");
	RootComponent = Mesh;
	TopCollider->SetupAttachment(RootComponent);
	BottomCollider->SetupAttachment(RootComponent);
	SplaterParticles->SetupAttachment(RootComponent);
	SmokeParticles->SetupAttachment(RootComponent);

	// set default variables 
	Temp = 0.0f;
	TimeStepCooking = 0.01f;
	TopColorLevel = 1.0f;
	BottomColorLevel = 1.0f;	
}

// Called when the game starts or when spawned
void ABurger::BeginPlay()
{
	Super::BeginPlay();
	
	// set particel effects to inactive
	SplaterParticles->SetActive(false, false);
	SmokeParticles->SetActive(false, false);

	// create timers for cooking
	GetWorldTimerManager().SetTimer(TopTimer, this, &ABurger::TopCook, TimeStepCooking, true, 0.0f);
	GetWorldTimerManager().PauseTimer(TopTimer);
	GetWorldTimerManager().SetTimer(BottomTimer, this, &ABurger::BottomCook, TimeStepCooking, true, 0.0f);
	GetWorldTimerManager().PauseTimer(BottomTimer);
}

// Called every frame
void ABurger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// if the burger gets burnt
	if(TopBurnt){
		if (SplaterParticles->IsActive()) {
			// change the particle from splatter to smoke
			SplaterParticles->SetActive(false, false);
			SmokeParticles->SetActive(true, false);
		}
	}

}

void ABurger::TopCook() {

	//make temp float
	float t;

	// get cooked level
	t = FMath::Clamp(TopCookedLevel + Temp, 0.0f, 1.0f);

	//set cooked level
	TopCookedLevel = t;

	//change material color
	TopMaterialColor->SetScalarParameterValue("Cooking", TopCookedLevel);

	//if within range
	if (TopCookedLevel >= 0.85f && TopCookedLevel <= 0.86f) {
		//set to burnt
		TopBurnt = true;
	}
}


void ABurger::BottomCook() {

	//make temp float
	float t;

	// get cooked level
	t = FMath::Clamp(BottomCookedLevel + Temp, 0.0f, 1.0f);

	//set cooked level
	BottomCookedLevel = t;

	//change material color
	BottomMaterialColor->SetScalarParameterValue("Cooking", BottomCookedLevel);
}


float ABurger::CalculateCost(bool burger) {

	//if contains burger
	if (burger) {
		//add to price
		Cost += 1.5f;
	}
	// if has a top bun
	if (BunColliderUsed) {
		// add to price
		Cost += 0.5f;
	}
	// if has a bottom bun
	if (BunCollider2Used) {
		// add to price
		Cost += 0.5f;
	}
	// if not burnt or undercooked
	if ((TopCookedLevel <= 0.6 && TopCookedLevel >= 0.4) && (BottomCookedLevel <= 0.6 && BottomCookedLevel >= 0.4)) {
		//add to price
		Cost += 2.5f;
	}

	//return price
	return Cost;

}

void ABurger::ChangeColor() {


}

void ABurger::OnOverlapEndTop(AActor* OverlappedActor, AActor* OtherActor)
{
	
}

void ABurger::OnOverlapEndBottom(AActor* OverlappedActor, AActor* OtherActor)
{
	
}
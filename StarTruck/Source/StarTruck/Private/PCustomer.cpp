// Fill out your copyright notice in the Description page of Project Settings.


#include "PCustomer.h"

// Sets default values
APCustomer::APCustomer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	OnActorBeginOverlap.AddDynamic(this, &APCustomer::OnOverlap);

	drinkType = DrinkType::Drink_None;

}

// Called when the game starts or when spawned
void APCustomer::BeginPlay()
{
	Super::BeginPlay();

	RandomiseOrder();

}

// Called every frame
void APCustomer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APCustomer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APCustomer::RandomiseOrder() {

	int32 swap = FMath::RandRange(0, 1);

	switch (swap) {

	case 0: {

		drink = true;
		int32 drinktype = FMath::RandRange(0, 2);

		switch (drinktype) {
		case 0: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
		case 1: {
			drinkType = DrinkType::Drink_Green;
			break;
		}
		case 2: {
			drinkType = DrinkType::Drink_Blue;
			break;
		}
		default: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
			   break;
		}

		int32 burgerRand = FMath::RandRange(0, 1);
		switch (burgerRand) {
		case 0: {
			burger = true;
			break;
		}
		case 1: {
		}
		default: {
			burger = false;
			break;
		}
			   break;
		}
		break;
	}
	case 1: {
		drink = false;
		burger = true;
		break;
	}
	default: {

		break;
	}
		   break;

	}


}

void APCustomer::GiveOrder() {

	
}

void APCustomer::FinishOrder() {

	if (!drink) {
		drinkCompleted = true;
	}
	if (!burger) {
		burgerCompleted = true;
	}

	TArray <AActor*> trayComponents;
	tray->GetAttachedActors(trayComponents, true);

	for (int32 i = 0; i != trayComponents.Num(); ++i)
	{
		if (trayComponents[i]->GetActorLocation() == tray->Mesh->GetSocketLocation("CupPoint")) {
			// Call drink cost counter
		}
		if (trayComponents[i]->GetActorLocation() == tray->Mesh->GetSocketLocation("BurgerPoint")) {
			ABurger* burgerhold = Cast<ABurger>(trayComponents[i]);
			cost += burgerhold->CalculateCost(burger);
		}
		if (trayComponents[i]->GetActorLocation() == tray->Mesh->GetSocketLocation("FriesPoint")) {
			// Call fries cost counter
		}
	}

	pay = true;
}

void APCustomer::Leave() {
	Destroy();
}

void APCustomer::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (Cast<ATray>(OtherActor) != nullptr)
	{
		tray = Cast<ATray>(OtherActor);
		FinishOrder();
	}
}



//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("This is an on screen message!"));

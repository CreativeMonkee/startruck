// Fill out your copyright notice in the Description page of Project Settings.


#include "VRController.h"
#include "Engine/GameEngine.h"

// Sets default values
AVRController::AVRController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup of blueprint components
	HandRoot = CreateDefaultSubobject<USceneComponent>(TEXT("HandOrigin"));
	HandRoot->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	HandRoot->SetRelativeScale3D(FVector::OneVector);

	RootComponent = HandRoot;

	MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MotionController"));
	MotionController->SetupAttachment(HandRoot);
	MotionController->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	MotionController->SetRelativeScale3D(FVector::OneVector);

	PhysicsHandle = CreateDefaultSubobject<UPhysicsHandleComponent>(TEXT("PhysicsHandle"));

	HandMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HandMesh"));
	HandMesh->SetupAttachment(MotionController);
	HandMesh->SetAutoActivate(true);
	HandMesh->SetVisibility(true);
	HandMesh->SetHiddenInGame(false);
	HandMesh->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	HandMesh->SetRelativeScale3D(FVector::OneVector);

	GrabSphere = CreateDefaultSubobject<USphereComponent>(TEXT("GrabSphere"));
	GrabSphere->SetupAttachment(MotionController);
	GrabSphere->SetGenerateOverlapEvents(true);
	GrabSphere->SetSphereRadius(10.0f);

	TeleportLaunchVelocity = 900.0f;

	isTeleporterActive = false;
	bLastFrameValidDestination = false;
	IsRoomScale = false;
	TeleportDestination = {0.0, 0.0, 0.0};
	IsValidTeleportDestination = false;
	GripState = EHandState::OPEN;
}

// Called when the game starts or when spawned
void AVRController::BeginPlay()
{

	Super::BeginPlay();

	FQuat qRotation = FQuat::Identity;
	FVector vec3Scale = FVector::OneVector;

	// Setup which hand this controller is based on the hand input given by the blueprint
	switch (Hand) {
	case EControllerHand::Left:
		// setup attachment to the motion controller
		MotionController->MotionSource = FXRMotionControllerBase::LeftHandSourceId;
		qRotation = FQuat(FVector(1, 0, 0), FMath::DegreesToRadians(90));
		vec3Scale = FVector(1, -1, 1);
		break;

	case EControllerHand::Right:
		// setup attachment to the motion controller
		MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
		qRotation = FQuat(FVector(1, 0, 0), FMath::DegreesToRadians(270));
		break;

	default:
		// default in case of an error
		MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
		qRotation = FQuat(FVector(1, 0, 0), FMath::DegreesToRadians(270));
		break;
	}

	MotionController->SetRelativeLocationAndRotation(FVector::ZeroVector, qRotation);
	MotionController->SetRelativeScale3D(vec3Scale);

	GrabbedObject = nullptr;

}

// Called every frame
void AVRController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsValid(GrabbedObject) && HasGrabbed) {
		if (FVector::Distance(HandMesh->GetComponentLocation(), GrabbedObject->GetComponentLocation()) <= 10 + OtherItemLocation) {
			HandleLocation = (HandMesh->GetComponentLocation() + HandMesh->GetForwardVector() * OtherItemLocation);
			FRotator NewRotation = HandMesh->GetComponentRotation();

			//NewRotation.Roll = HandMesh->GetComponentRotation().Roll * -1.0f - 90.0f;
			PhysicsHandle->SetTargetLocationAndRotation(HandleLocation, NewRotation);
		}
		else {
			ReleaseActor();
		}
		
	}

}

void AVRController::GrabActor() {
	bWantsToGrip = true;

	AActor* Actor = nullptr;

	GetActorNearHand(Actor);

	if (IsValid(Actor)) {

		AttachedActor = Actor;
		IPickupInterface::Execute_Pickup(AttachedActor, MotionController);

		OtherItemLocation = (HandMesh->GetComponentLocation() - Actor->GetActorLocation()).Size() ;
		OtherItemRotation = HandMesh->GetComponentRotation();

		GrabbedObject = Cast<UPrimitiveComponent>(Actor->GetRootComponent());

		PhysicsHandle->GrabComponentAtLocationWithRotation(GrabbedObject, TEXT("NONE"), Actor->GetActorLocation(), HandMesh->GetComponentRotation());

		HasGrabbed = true;

		//Old pickup code
		//AttachedActor = Actor;
		//IPickupInterface::Execute_Pickup(AttachedActor, MotionController);
	}

}

void AVRController::ReleaseActor() {
	bWantsToGrip = false;

	if (IsValid(GrabbedObject)) {
		
		PhysicsHandle->ReleaseComponent();
		IPickupInterface::Execute_Drop(AttachedActor);

		GrabbedObject = nullptr;
		AttachedActor = nullptr;
		HasGrabbed = false;

		//Old drop code
		//if (AttachedActor->K2_GetRootComponent()->GetAttachParent() == MotionController) {
		//	IPickupInterface::Execute_Drop(AttachedActor);
		//	AttachedActor = nullptr;
		//}
	}
}

void AVRController::GetActorNearHand(AActor* &NearestActor) {
	
	TArray<AActor*> OverlappingActors;

	AActor* NearestOverlappingActor = nullptr;
	
	FVector SphereWorldLocation;

	float NearestOverlap = 10000.0f;

	GrabSphere->GetOverlappingActors(OverlappingActors, AActor::StaticClass());

	SphereWorldLocation = GrabSphere->GetComponentLocation();
	
	if (OverlappingActors.Num() != 0) {
		for (AActor* Actor : OverlappingActors) {
			if (Actor->GetClass()->ImplementsInterface(UPickupInterface::StaticClass())) {
				if ((Actor->GetActorLocation() - SphereWorldLocation).Size() < NearestOverlap) {
					NearestOverlappingActor = Actor;
					NearestOverlap = (Actor->GetActorLocation() - SphereWorldLocation).Size();
				}
			}
		}
	}
	NearestActor = NearestOverlappingActor;
}

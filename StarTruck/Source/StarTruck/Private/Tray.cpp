// Fill out your copyright notice in the Description page of Project Settings.


#include "Tray.h"

// Sets default values
ATray::ATray()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	//create components and attach to root
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	CupMesh = CreateDefaultSubobject<UStaticMeshComponent>("CupMesh");
	HotDogMesh = CreateDefaultSubobject<UStaticMeshComponent>("HotDogMesh");
	BurgerMesh = CreateDefaultSubobject<UStaticMeshComponent>("BurgerMesh");
	Collider = CreateDefaultSubobject<UBoxComponent>("Collider");
	RootComponent = Mesh;
	Collider->SetupAttachment(RootComponent);
	CupMesh->SetupAttachment(RootComponent);
	HotDogMesh->SetupAttachment(RootComponent);
	BurgerMesh->SetupAttachment(RootComponent);

	//bind overlap event
	OnActorBeginOverlap.AddDynamic(this, &ATray::OnOverlap);

}

// Called when the game starts or when spawned
void ATray::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATray::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATray::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	// create attachment rules
	FAttachmentTransformRules attach = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, true);

	// if the overlapped actor is a cup
	if (Cast<ACup>(OtherActor) != nullptr)
	{
		// cast to cup
		ACup* temp = Cast<ACup>(OtherActor);
		//dissable physics and bind to tray
		temp->DisableComponentsSimulatePhysics();
		temp->AttachToActor(OverlappedActor, attach, "CupSocket");
		//disable hologram
		if (CupMesh->IsVisible()) {
			CupMesh->SetVisibility(false);
			//set order components for pay
			orderComp.drink = true;
			orderComp.drinkFillLevel = 1.0f;
		}
	}
	// if the overlapped actor is a burger
	if (Cast<ABurger>(OtherActor) != nullptr) {

		// cast to burger
		ABurger* temp = Cast<ABurger>(OtherActor);
		//dissable physics and bind to tray
		temp->DisableComponentsSimulatePhysics();
		temp->AttachToActor(OverlappedActor, attach, "BurgerSocket");
		//disable hologram
		if (BurgerMesh->IsVisible()) {
			BurgerMesh->SetVisibility(false);
			//set order components for pay
			orderComp.burger = true;
			orderComp.burgerCookedLevel = temp->TopCookedLevel;
			orderComp.burgerTopBun = temp->BunColliderUsed;
			orderComp.burgerBottomBun = temp->BunCollider2Used;
		}
	}
	// if the overlapped actor is a hot dog
	if (Cast<AHotDog>(OtherActor) != nullptr) {

		// cast to hotdog
		AHotDog* temp = Cast<AHotDog>(OtherActor);
		//dissable physics and bind to tray
		OtherActor->DisableComponentsSimulatePhysics();
		temp->AttachToActor(OverlappedActor, attach, "FriesSocket");
		//disable hologram
		if (HotDogMesh->IsVisible()) {
			HotDogMesh->SetVisibility(false);
			//set order components for pay
			orderComp.hotDog = true;
			orderComp.hotDogBun = temp->BunColliderUsed;
			orderComp.hotDogCookedLevel = temp->CookedLevel;
		}
	}
}

void ATray::TakeOrder(bool drink, bool burger,bool hotDog, DrinkType drinktype) {

	//if order contains drink
	if (drink) {
		//set hologram to visable
		CupMesh->SetVisibility(true);
	}
	//if order contains burger
	if (burger) {
		//set hologram to visable
		BurgerMesh->SetVisibility(true);
	}
	//if order contains hotdog
	if (hotDog) {
		//set hologram to visable
		HotDogMesh->SetVisibility(true);
	}
}
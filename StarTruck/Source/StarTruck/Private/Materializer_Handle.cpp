// Fill out your copyright notice in the Description page of Project Settings.


#include "Materializer_Handle.h"

// Sets default values
AMaterializer_Handle::AMaterializer_Handle()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Origin = CreateDefaultSubobject<USceneComponent>(TEXT("HandleOrigin"));
	Origin->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	Origin->SetRelativeScale3D(FVector::OneVector);

	RootComponent = Origin;

	MainMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MainMesh"));
	MainMesh->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	MainMesh->SetRelativeScale3D(FVector::OneVector);
	MainMesh->SetupAttachment(Origin);

	HandleOverlap = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComponent"));
	HandleOverlap->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	HandleOverlap->SetRelativeScale3D(FVector::OneVector);
	HandleOverlap->SetupAttachment(MainMesh);


}

// Called when the game starts or when spawned
void AMaterializer_Handle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMaterializer_Handle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


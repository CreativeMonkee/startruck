// Fill out your copyright notice in the Description page of Project Settings.


#include "Grill.h"

// Sets default values
AGrill::AGrill()
{

 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	//set defult value
	Temperature = 0.0005f;

}

// Called when the game starts or when spawned
void AGrill::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGrill::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGrill::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

// Fill out your copyright notice in the Description page of Project Settings.


#include "CCustomer.h"

// Sets default values
ACCustomer::ACCustomer()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	//OnActorBeginOverlap.AddDynamic(this, &ACCustomer::OnOverlap);

	//set default variables
	drinkType = DrinkType::Drink_None;
	impatience = 90.0f;

}

// Called when the game starts or when spawned
void ACCustomer::BeginPlay()
{
	Super::BeginPlay();

	//create and pause timer
	GetWorldTimerManager().SetTimer(orderTimer, this, &ACCustomer::tickDown, 1.0f, true, 0.0f);
	GetWorldTimerManager().PauseTimer(orderTimer);
}

// Called every frame
void ACCustomer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACCustomer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ACCustomer::tickDown() {

	//tick down customer impatience
	impatience -= 1.0f;
	//if impatience is out
	if (impatience <= 0.0f) {
		//customer leaves
		Leave();
	}
}

void ACCustomer::RandomiseOrder() {

	//get the world time
	float worldTime = GetWorld()->GetTimeSeconds();
	int32 swap;

	// if it is less then 1 and a half minutes
	if (worldTime <= 90.0f) {
		//only spawn first 3 orders
		swap = FMath::RandRange(0, 3);
	}
	//if between 1.5 minutes and 3.5 minutes
	if (worldTime <= 210.0f && worldTime > 90.0f) {
		//give the option for 7 orders
		swap = FMath::RandRange(0, 7);
	}
	//if the time is over 3.5 minutes
	if (worldTime > 210.0f) {
		//give the option for all 9 orders
		swap = FMath::RandRange(0, 9);

	}

	//change order
	switch (swap) {

	case 0: {
		// order contains drink
		drink = true;
		int32 drinktype = FMath::RandRange(0, 2);

		//select drink type
		switch (drinktype) {
		case 0: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
		case 1: {
			drinkType = DrinkType::Drink_Green;
			break;
		}
		case 2: {
			drinkType = DrinkType::Drink_Blue;
			break;
		}
		default: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
			   break;
		}
		break;
	}
	case 1: {
		// order contains burger
		burger = true;
		break;
	}
	case 2: {
		// order contains hotdog
		hotDog = true;
		break;
	}
	case 3: {
		// order contains ice cream
		iceCream = true;
		break;
	}
	case 4: {
		// order contains icecream. Duplicate to make it more likely
		iceCream = true;
		break;
	}
	case 5: {
		// order contains burger
		burger = true;
		// order contains drink
		drink = true;
		int32 drinktype = FMath::RandRange(0, 2);
		//select drink type
		switch (drinktype) {
		case 0: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
		case 1: {
			drinkType = DrinkType::Drink_Green;
			break;
		}
		case 2: {
			drinkType = DrinkType::Drink_Blue;
			break;
		}
		default: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
		break;
		}
		break;

	}
	case 6: {
		// order contains burger
		burger = true;
		//order contains hotdog
		hotDog = true;
		break;
	}
	case 7: {
		// order contains drink
		drink = true;
		int32 drinktype = FMath::RandRange(0, 2);
		//select drink type
		switch (drinktype) {
		case 0: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
		case 1: {
			drinkType = DrinkType::Drink_Green;
			break;
		}
		case 2: {
			drinkType = DrinkType::Drink_Blue;
			break;
		}
		default: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
			   break;
		}
		// order contains hotdog
		hotDog = true;
		break;
	}
	case 8: {
		// order contains drink
		drink = true;
		int32 drinktype = FMath::RandRange(0, 2);
		//select drink type
		switch (drinktype) {
		case 0: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
		case 1: {
			drinkType = DrinkType::Drink_Green;
			break;
		}
		case 2: {
			drinkType = DrinkType::Drink_Blue;
			break;
		}
		default: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
			   break;
		}
		// order contains burger
		burger = true;
		// order contains burger
		hotDog = true;
		break;
	}
	case 9: {
		// order contains drink
		drink = true;
		int32 drinktype = FMath::RandRange(0, 2);
		//select drink type
		switch (drinktype) {
		case 0: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
		case 1: {
			drinkType = DrinkType::Drink_Green;
			break;
		}
		case 2: {
			drinkType = DrinkType::Drink_Blue;
			break;
		}
		default: {
			drinkType = DrinkType::Drink_Red;
			break;
		}
			   break;
		}
		// order contains burger
		burger = true;
		// order contains burger
		hotDog = true;
		break;
	}
	default: {
		// order contains icecream
		iceCream = true;
		break;
	}
	}

}

void ACCustomer::GiveOrder() {

	//randomise the customers order
	RandomiseOrder();

	//cast to order machine
	AOrderMachine* oMachine = Cast<AOrderMachine>(orderMachineref);

	//if its not null
	if (orderMachineref != nullptr) {
		//unpause the order timer
		GetWorldTimerManager().UnPauseTimer(orderTimer);
		//pass the order to the machine
		oMachine->TakeOrder(iceCream, drink, burger, hotDog, drinkType, SittingNumber, this);
	}
}

float ACCustomer::FinishOrder(FOrderComponents orderComp) {

	//pause the imer
	GetWorldTimerManager().PauseTimer(orderTimer);

	//reset the variables so same item is not added to pay multiple times
	if (!drink) {
		drinkCompleted = true;
	}
	if (!burger) {
		burgerCompleted = true;
	}
	if (!iceCream) {
		iceCreamCompleted = true;
	}
	if (!hotDog) {
		hotDogCompleted = true;
	}

	//if no drink has been processed
	if (!drinkCompleted) {
		//if tray has the drink
		if (orderComp.drink) {
			// add to pay
			pay += 3.0f;
		}
		//if the drink is almost completely full
		if (orderComp.drinkFillLevel >= 0.8) {
			// add to pay
			pay += 2.0f;
		}
	}
	// if no burger has been processed
	if (!burgerCompleted) {
		// if tray contains burger
		if (orderComp.burger) {
			// add to pay
			pay += 1.5f;
		}
		//if burger has a top bun
		if (orderComp.burgerTopBun) {
			//add to pay
			pay += 0.5f;
		}
		//if the burger has a bottom bun
		if (orderComp.burgerBottomBun) {
			// add to pay
			pay += 0.5f;
		}
		//if the burger is not under cooked or over cooked
		if (orderComp.burgerCookedLevel >= 0.6 && orderComp.burgerCookedLevel <= 0.85) {
			//add to pay
			pay += 2.5f;
		}
	}
	//if no hotdog has been processeed
	if (!hotDogCompleted) {
		// if the tray has a hotdog
		if (orderComp.hotDog) {
			// add to pay
			pay += 1.5f;
		}
		//if the tray has a hot dog bun
		if (orderComp.hotDogBun) {
			//add to pay
			pay += 1.0f;
		}
		//if the hot dog is not undercooked or burnt
		if (orderComp.hotDogCookedLevel <= 0.85 && orderComp.hotDogCookedLevel >= 0.6) {
			//add to pay
			pay += 2.5f;
		}
	}

	//return the payment 
	return pay;
}

void ACCustomer::Leave() {
}
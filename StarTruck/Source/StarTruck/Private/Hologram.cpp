// Fill out your copyright notice in the Description page of Project Settings.


#include "Hologram.h"

// Sets default values
AHologram::AHologram()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//create class components and bind to root component
	Root = CreateDefaultSubobject<UStaticMeshComponent>("Root");
	IceCreamMesh = CreateDefaultSubobject<UStaticMeshComponent>("IceCreamMesh");
	HologramBeamMesh = CreateDefaultSubobject<UStaticMeshComponent>("HologramBeamMesh");
	HologramBaseMesh = CreateDefaultSubobject<UStaticMeshComponent>("HologramBaseMesh");
	RootComponent = Root;
	IceCreamMesh->SetupAttachment(RootComponent);
	HologramBeamMesh->SetupAttachment(RootComponent);
	HologramBaseMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AHologram::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHologram::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHologram::SpawnHologram() {

	//set the hologram visability to true
	IceCreamMesh->SetVisibility(true);
	HologramBeamMesh->SetVisibility(true);
}
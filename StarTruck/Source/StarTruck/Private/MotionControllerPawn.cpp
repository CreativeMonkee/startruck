// Fill out your copyright notice in the Description page of Project Settings.


#include "MotionControllerPawn.h"

// Sets default values
AMotionControllerPawn::AMotionControllerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("VRRoot"));
	SceneComponent->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	SceneComponent->SetRelativeScale3D(FVector::OneVector);
	RootComponent = SceneComponent;

	VROrigin = CreateDefaultSubobject<USceneComponent>(TEXT("VROrigin"));
	VROrigin->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	VROrigin->SetRelativeScale3D(FVector::OneVector);
	VROrigin->SetupAttachment(SceneComponent);


	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	Camera->SetRelativeScale3D(FVector::OneVector);
	Camera->SetupAttachment(VROrigin);
	
}

// Called when the game starts or when spawned
void AMotionControllerPawn::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMotionControllerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMotionControllerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


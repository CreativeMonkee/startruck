// Fill out your copyright notice in the Description page of Project Settings.


#include "Materializer.h"


// Sets default values
AMaterializer::AMaterializer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	
	PrimaryActorTick.bCanEverTick = true;

	// Set up the meshes and overlaps
	MainMachine = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MainMachine"));
	MainMachine->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	MainMachine->SetRelativeScale3D(FVector::OneVector);

	RootComponent = MainMachine;

	HologramCylinder = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HologramCylinder"));
	HologramCylinder->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	HologramCylinder->SetRelativeScale3D(FVector::OneVector);
	HologramCylinder->SetupAttachment(MainMachine);


	Beam = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Beam"));
	Beam->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	Beam->SetRelativeScale3D(FVector::OneVector);
	Beam->SetupAttachment(MainMachine);

	Handle = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Handle"));
	Handle->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	Handle->SetRelativeScale3D(FVector::OneVector);
	Handle->SetupAttachment(MainMachine);

	
	Button_L = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Button_L"));
	Button_L->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	Button_L->SetRelativeScale3D(FVector::OneVector);
	Button_L->SetupAttachment(MainMachine);
	Button_L->OnComponentBeginOverlap.AddDynamic(this, &AMaterializer::OnBeginOverlapLButton);

	Button_R = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Button_R"));
	Button_R->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	Button_R->SetRelativeScale3D(FVector::OneVector);
	Button_R->SetupAttachment(MainMachine);
	Button_R->OnComponentBeginOverlap.AddDynamic(this, &AMaterializer::OnBeginOverlapRButton);

	Spawn = CreateDefaultSubobject<USphereComponent>(TEXT("Spawn"));
	Spawn->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	Spawn->SetRelativeScale3D(FVector::OneVector);
	Spawn->SetupAttachment(MainMachine);

	HoloMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HoloMesh"));
	HoloMesh->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	HoloMesh->SetRelativeScale3D(FVector::OneVector);
	HoloMesh->SetupAttachment(Spawn);

	HandleOverlap = CreateDefaultSubobject<UCapsuleComponent>(TEXT("HandleOverlap"));
	HandleOverlap->SetRelativeLocationAndRotation(FVector::ZeroVector, FQuat::Identity);
	HandleOverlap->SetRelativeScale3D(FVector::OneVector);
	HandleOverlap->SetupAttachment(Handle);
	HandleOverlap->OnComponentBeginOverlap.AddDynamic(this, &AMaterializer::OnBeginOverlapHandle);
	

	// set up the blueprints that will be spawned which need to be entered in the blueprint for this machine.
	
	if (Selections.Num() > 0) {
		SelectedItem = Selections[0];
		SelectionValue = 0;

	}	
}

// Called when the game starts or when spawned
void AMaterializer::BeginPlay()
{
	Super::BeginPlay();

	// set up the blueprints that will be spawned which need to be entered in the blueprint for this machine.
	// this is in twice as this allows the engine to see it in the game and outside.
	if (Selections.Num() > 0) {
		SelectedItem = Selections[0];
		SelectionValue = 0;

	}
}



// Called every frame
void AMaterializer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Overlap with handle will spawn the selected item from the list or items
// I have made this so it is easy to implement new objects to spawn so it is able to spawn anything that extends from the Actor class.
void AMaterializer::OnBeginOverlapHandle(UPrimitiveComponent* OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult){
	AVRController* test = Cast<AVRController>(OtherActor);

	if (test != nullptr) {
		// Spawn Item
		
		if (SelectedItem != nullptr) {

			FVector SpawnLocation = Spawn->GetComponentLocation();
			FRotator SpawnRotation = Spawn->GetComponentRotation();
			FVector SpawnScale = FVector::OneVector;

			FTransform NewTransform = FTransform(SpawnRotation, SpawnLocation, SpawnScale);
			
			FActorSpawnParameters SpawnParams;

			//Spawn a copy of the currently selected item from the selections list
			GetWorld()->SpawnActor<AActor>(SelectedItem, NewTransform, SpawnParams);
		} else {

		}

	}
}

// Moves through the possible spawn items in the "right" direction and displays the selected item in the hologram
void AMaterializer::OnBeginOverlapRButton(UPrimitiveComponent* OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {
	AVRController* test = Cast<AVRController>(OtherActor);

	if (test != nullptr) {
		// Change Item Right
		if (Selections.Num() > 0) {
			SelectionValue++;
			if (SelectionValue >= Selections.Num()) {
				SelectionValue = 0;
			}

			SelectedItem = Selections[SelectionValue];

			FVector SpawnLocation = Spawn->GetComponentLocation();
			FRotator SpawnRotation = Spawn->GetComponentRotation();
			FVector SpawnScale = FVector::OneVector;

			FTransform NewTransform = FTransform(SpawnRotation, SpawnLocation, SpawnScale);

			FActorSpawnParameters SpawnParams;

			//spawns actor to get reference to the mesh to create a copy of the mesh as I was unable to do it any other way.
			//the actor is then deleted after the if statement within the same frame so that it only shows the mesh up and the item is not interactable.
			AActor* Ref = GetWorld()->SpawnActor<AActor>(SelectedItem, NewTransform, SpawnParams);

			Ref->SetActorEnableCollision(false);
			UActorComponent* Mesh = Ref->GetComponentByClass(UStaticMeshComponent::StaticClass());

			if (Mesh != nullptr) {
				UStaticMeshComponent* MeshTest = Cast<UStaticMeshComponent>(Mesh);
				if (MeshTest != nullptr) {
					HoloMesh->SetStaticMesh(MeshTest->GetStaticMesh());
					HoloMesh->SetMaterial(0, HoloMaterial);
					Ref->Destroy();
				}
			} else {
				Ref->Destroy();
			}

		}

	}
}

// Moves through the possible spawn items in the "left" direction and displays the selected item in the hologram
void AMaterializer::OnBeginOverlapLButton(UPrimitiveComponent* OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {
	AVRController* test = Cast<AVRController>(OtherActor);

	if (test != nullptr) {
		// Change Item Left
		if (Selections.Num() > 0) {
			SelectionValue--;
			if (SelectionValue < 0) {
				SelectionValue = Selections.Num() - 1;
			}

			SelectedItem = Selections[SelectionValue];

			FVector SpawnLocation = Spawn->GetComponentLocation();
			FRotator SpawnRotation = Spawn->GetComponentRotation();
			FVector SpawnScale = FVector::OneVector;

			FTransform NewTransform = FTransform(SpawnRotation, SpawnLocation, SpawnScale);

			FActorSpawnParameters SpawnParams;


			//spawns actor to get reference to the mesh to create a copy of the mesh as I was unable to do it any other way.
			//the actor is then deleted after the if statement within the same frame so that it only shows the mesh up and the item is not interactable.
			AActor* Ref = GetWorld()->SpawnActor<AActor>(SelectedItem, NewTransform, SpawnParams);

			Ref->SetActorEnableCollision(false);
			UActorComponent* Mesh = Ref->GetComponentByClass(UStaticMeshComponent::StaticClass());

			if (Mesh != nullptr) {
				UStaticMeshComponent* MeshTest = Cast<UStaticMeshComponent>(Mesh);
				if (MeshTest != nullptr) {
					HoloMesh->SetStaticMesh(MeshTest->GetStaticMesh());
					HoloMesh->SetMaterial(0, HoloMaterial);
					Ref->Destroy();
				}
			} else {
				Ref->Destroy();
			}

		}
		
	}
	
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "HotDog.h"

// Sets default values
AHotDog::AHotDog()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//bind the overlap events to trigger
	OnActorEndOverlap.AddDynamic(this, &AHotDog::OnOverlapEndTop);
	OnActorEndOverlap.AddDynamic(this, &AHotDog::OnOverlapEndBottom);

	//create class components and bind to root component
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Collider = CreateDefaultSubobject<UCapsuleComponent>("Collider");
	SplaterParticles = CreateDefaultSubobject<UParticleSystemComponent>("SplaterParticles");
	SmokeParticles = CreateDefaultSubobject<UParticleSystemComponent>("SmokeParticles");
	RootComponent = Mesh;
	Collider->SetupAttachment(RootComponent);
	SplaterParticles->SetupAttachment(RootComponent);
	SmokeParticles->SetupAttachment(RootComponent);

	// set default variables 
	Temp = 0.0f;
	TimeStepCooking = 0.01f;
	ColorLevel = 1.0f;
}

// Called when the game starts or when spawned
void AHotDog::BeginPlay()
{
	Super::BeginPlay();

	// set particel effects to inactive
	SplaterParticles->SetActive(false, false);
	SmokeParticles->SetActive(false, false);

	// create timers for cooking
	GetWorldTimerManager().SetTimer(Timer, this, &AHotDog::Cook, TimeStepCooking, true, 0.0f);
	GetWorldTimerManager().PauseTimer(Timer);
}

// Called every frame
void AHotDog::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// if the hotdog gets burnt
	if (Burnt) {
		if (SplaterParticles->IsActive()) {
			// change the particle from splatter to smoke
			SplaterParticles->SetActive(false, false);
			SmokeParticles->SetActive(true, false);
		}
	}
}


void AHotDog::Cook() {

	//make temp float
	float t;

	// get cooked level
	t = FMath::Clamp(CookedLevel + Temp, 0.0f, 1.0f);

	//set cooked level
	CookedLevel = t;

	//change material color
	MaterialColor->SetScalarParameterValue("Cooking", CookedLevel);

	//if within range
	if (CookedLevel >= 0.85f && CookedLevel <= 0.86f) {
		//set to burnt
		Burnt = true;
	}
}

float AHotDog::CalculateCost(bool hotDog) {

	//if contains hotdog
	if (hotDog) {
		//add to price
		Cost += 1.5f;
	}
	//if it has a bun
	if (BunColliderUsed) {
		//add to price
		Cost += 1.0f;
	}
	// if not burnt or undercooked
	if (CookedLevel <= 0.6 && CookedLevel >= 0.4) {
		//add to price
		Cost += 2.5f;
	}
	//return price
	return Cost;

}

void AHotDog::ChangeColor() {


}

void AHotDog::OnOverlapEndTop(AActor* OverlappedActor, AActor* OtherActor)
{

}

void AHotDog::OnOverlapEndBottom(AActor* OverlappedActor, AActor* OtherActor)
{

}
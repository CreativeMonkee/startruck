// Fill out your copyright notice in the Description page of Project Settings.


#include "IceCream.h"

// Sets default values
AIceCream::AIceCream()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//create class components and bind to root component
	IceCream = CreateDefaultSubobject<UStaticMeshComponent>("IceCreamMesh");
	Collider = CreateDefaultSubobject<UCapsuleComponent>("Collider");
	IceCream->SetupAttachment(RootComponent);
	Collider->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AIceCream::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AIceCream::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Fill out your copyright notice in the Description page of Project Settings.


#include "OrderMachine.h"

// Sets default values
AOrderMachine::AOrderMachine()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AOrderMachine::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AOrderMachine::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AOrderMachine::TakeOrder(bool iceCream, bool drink, bool burger,bool hotDog, DrinkType drinktype, int32 SittingPoint, ACCustomer* Customer) {

	//if the order is just an icecream
	if (iceCream) {
		//enable the hologram in front of the customer
		AHologram* hologram = Cast<AHologram>(hologramSpawns[SittingPoint]);
		if (hologram != nullptr) {
			hologram->SpawnHologram();
		}
	}
	//otherwise
	else {
		// create the tray in front of the customer
		ATrayCompartment* tray = Cast<ATrayCompartment>(traySpawns[SittingPoint]);
		if (tray != nullptr) {
			// send the order to the tray
			Customer->tray = tray->SpawnTray(drink, burger, hotDog, drinktype);
		}
	}
}